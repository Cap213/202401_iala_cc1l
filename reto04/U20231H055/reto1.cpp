#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int* aleatorio(int& size) {
	size = 100 + rand() % 401; // Tama�o aleatorio entre 100 y 500
	int* arreglo = new int[size]; // Arreglo din�mico
	for (int i = 0; i < size; ++i) {
		arreglo[i] = 1 + rand() % 10000; // Elementos entre 1 y 10000
	}
	return arreglo;
}

void imprimir(int* arreglo, int size) {
	for (int i = 0; i < size; ++i) {
		cout << arreglo[i] << " ";
	}
	cout << endl;
}


int main() {
	srand(time(0));
	int size;
	int* arreglo = aleatorio(size);
	cout << "Tama�o del arreglo: " << size << endl;
	cout << "Elementos del arreglo: " << endl;
	imprimir(arreglo, size); 
	delete[] arreglo;
	
	cout << "hola mundo";
	system("pause");
	return 0;
}