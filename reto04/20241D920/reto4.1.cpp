#include <iostream>
#include <conio.h>
using namespace std;

int** arreglo(int n, int m);
void imprimir(int** arreglo, int n, int m);

int main() {
	int n, m;
	cout << "Ingrese el numero de filas del arreglo n x m: ";
	cin >> n;
	cout << "Ingrese el numero de columnas del arreglo n x m: ";
	cin >> m;
	
	int** arr = arreglo(n, m);
	imprimir(arr, n, m);
	
	for (int i = 0; i < n; ++i) {
		delete[]arr[i];
	}
	delete[]arr;
	_getch();
	return 0;
}

int** arreglo(int n, int m) {
	int** matriz = new int* [n];
	for (int i = 0; i < n; ++i) {
		matriz[i] = new int[m];
		for (int j = 0; j < m; ++j) {
			matriz[i][j] = 0;
			
		}
	}
	return matriz;
}

void imprimir(int** arreglo, int n, int m) {
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			cout << arreglo[i][j];
		}
		cout << endl;
	}
}
